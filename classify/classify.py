import argparse
import os
import sys
sys.path.insert(0, 'genbed')

import pandas as pd
import numpy as np
import matplotlib, matplotlib.pyplot as plt

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

# Classifiers
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis

import genbed.classification as gclass
import genbed.statistics as gstats
import genbed.visualisation as gvis

def create_latex_report_table(metrics_summary, classifier_name):
        header = ('\\begin{table}[H]\n' +
                  '\centering' +
                  '\\begin{tabular}{|l|lllll|}\n' + 
                  '\hline\n' + 
                  'Generative Model & Precision & Recall & $F_1$ Score & Accuracy & AUC \\\\ \hline\n')
        body = ''
        for row in metrics_summary:
                rounded = [round(row[i], 2) for i in range(1, len(row))]
                if 'rdcm' in row[0]:
                        num_components = row[0][5:]
                        body += f'rDCM ({num_components} ICs) & {rounded[0]} & {rounded[1]} & {rounded[2]} & {rounded[3]} & {rounded[4]} \\\\ \n'
                elif 'func_conn' in row[0]:
                        num_components = row[0][10:]
                        body += f'Func. Conn. ({num_components} ICs) & {rounded[0]} & {rounded[1]} & {rounded[2]} & {rounded[3]} & {rounded[4]} \\\\ \n'

                else:
                        body += f'S{row[0][1:]} (6 ICs) & {rounded[0]} & {rounded[1]} & {rounded[2]} & {rounded[3]} & {rounded[4]} \\\\ \n'
        tail = ('\hline\n' +
                '\end{tabular}\n' +
                '\caption{Generative embedding results with ' + classifier_name + '.}\n' +
                '\end{table}')
        return header + body + tail

if __name__ == '__main__':
        parser = argparse.ArgumentParser()
        parser.add_argument(
                '-features',
                type=str,
                default='rdcm_6',
                help='which feature set to use'
        )
        parser.add_argument(
                '-classifier',
                type=str,
                default='sigmoid_svm',
                help='type of classifier to use'
        )
        args = parser.parse_args()

        # Set random seed for reproducibility
        np.random.seed(42)

        # Load features
        if args.features == 'all':
                gen_model_names = ['func_conn_6', 'func_conn_21', 'func_conn_55', 'stochastic', 'spectral', 'rdcm_6', 'rdcm_21', 'rdcm_55']
                gen_models_X = [pd.read_csv(f'../dcm/output/{gen_model_name}_X.csv') for gen_model_name in gen_model_names]
        else:
                gen_model_name = args.features
                gen_model_X = pd.read_csv(f'../dcm/output/{gen_model_name}_X.csv')

        labels = pd.read_csv('../dcm/output/y_train.csv', squeeze=True).astype('category') # squeeze=True

        # All functions allow the removal of a set of confounds via linear regression
        confounds = pd.read_csv('../dataset/train_confounds.csv', index_col='EID')
        confounds['Gender'] = confounds['Gender'].astype('category').cat.codes
        confounds['Handedness'] = confounds['Handedness'].astype('category').cat.codes
        confounds['Smoking'] = confounds['Smoking'].astype('category').cat.codes
        confounds['Cannabis'] = confounds['Cannabis'].astype('category').cat.codes
        confounds['Drug Addiction'] = confounds['Drug Addiction'].astype('category').cat.codes
        confounds['Alcohol'] = confounds['Alcohol'].astype('category').cat.codes

        # Run classification
        # `predict()` performs nested cross-validation
        if args.classifier == 'rbf_svm':
                classifier = SVC(probability=True)
                parameters = {
                        'C': [0.01, 0.1, 1],
                        'gamma': [1, 0.1, 0.01, 0.001],
                        'kernel': ['rbf']
                }
        elif args.classifier == 'linear_svm':
                classifier = SVC(probability=True)
                parameters = {
                        'C': [0.01, 0.1, 1],
                        'gamma': [1, 0.1, 0.01, 0.001],
                        'kernel': ['linear']
                }
        elif args.classifier == 'poly_svm_3':
                classifier = SVC(probability=True)
                parameters = {
                        'C': [0.01, 0.1, 1],
                        'gamma': [1, 0.1, 0.01, 0.001],
                        'kernel': ['poly'],
                        'degree': [3]
                }
        elif args.classifier == 'poly_svm_4':
                classifier = SVC(probability=True)
                parameters = {
                        'C': [0.01, 0.1, 1],
                        'gamma': [1, 0.1, 0.01, 0.001],
                        'kernel': ['poly'],
                        'degree': [4]
                }
        elif args.classifier == 'poly_svm_5':
                classifier = SVC(probability=True)
                parameters = {
                        'C': [0.01, 0.1, 1],
                        'gamma': [1, 0.1, 0.01, 0.001],
                        'kernel': ['poly'],
                        'degree': [5]
                }
        elif args.classifier == 'sigmoid_svm':
                classifier = SVC(probability=True)
                parameters = {
                        'C': [0.01, 0.1, 1],
                        'gamma': [1, 0.1, 0.01, 0.001],
                        'kernel': ['sigmoid']
                }
        elif args.classifier == 'logres':
                classifier, parameters = gclass.get_default_classifier()
        elif args.classifier == 'rf':
                classifier = RandomForestClassifier()
                parameters = {
                        'n_estimators': [100, 500, 1000],
                        'max_depth': [10, 30, 60]
                }
        elif args.classifier == 'knn':
                classifier = KNeighborsClassifier()
                parameters = {
                        'n_neighbors': [3, 5, 7, 9],
                        'leaf_size': [20, 30, 40]
                }
        elif args.classifier == 'nn_shallow':
                classifier = MLPClassifier()
                parameters = {
                        'max_iter': [500],
                        'hidden_layer_sizes': [(100,), (150,), (300,), (500,)]
                }
        elif args.classifier == 'nn_medium':
                classifier = MLPClassifier()
                parameters = {
                        'max_iter': [500],
                        'hidden_layer_sizes': [(100, 50), (150, 20), (300, 100), (500, 250)]
                }
        elif args.classifier == 'nn_deep':
                classifier = MLPClassifier()
                parameters = {
                        'max_iter': [1000],  # Allows time for deep net to converge
                        'hidden_layer_sizes': [(100, 50, 5), (150, 20, 10), (500, 250, 50)]
                }
        elif args.classifier == 'gpc':
                classifier = GaussianProcessClassifier()
                parameters = {
                        'max_iter_predict': [300]
                }
        elif args.classifier == 'dtc':
                classifier = DecisionTreeClassifier()
                parameters = {}
        elif args.classifier == 'ada':
                classifier = AdaBoostClassifier()
                parameters = {
                        'n_estimators': [30, 50, 70]
                }
        elif args.classifier == 'gnb':
                classifier = GaussianNB()
                parameters = {}
        elif args.classifier == 'qda':
                classifier = QuadraticDiscriminantAnalysis()
                parameters = {}
        elif args.classifier == 'gdc':
                classifier = GradientBoostingClassifier()
                parameters = {
                        'n_estimators': [50, 100, 150]
                }

        if args.features == 'all':
                os.makedirs(f'report/{args.classifier}', exist_ok=True)
                metrics_summary = []
                for i, gen_model_name in enumerate(gen_model_names):
                        print(f'Training {gen_model_name} with {args.classifier}')
                        predictions, probabilities, roc_fig, mean_auc = gclass.parallel_predict(
                                gen_models_X[i], labels, classifier, parameters,
                                confounds=confounds, return_probabilities=True, inner_cv_kwargs={'n_splits': 5})
                        print(f'{gen_model_name} performance with {args.classifier}')
                        precision, recall, f1_score, acc = gclass.evaluate_performance(labels, predictions)
                        metrics_summary.append((gen_model_name, precision, recall, f1_score, acc, mean_auc))
                        metrics_table = create_latex_report_table(metrics_summary, classifier.__class__.__name__)
                        with open(f'report/{args.classifier}/report_table_{args.classifier}.txt', 'w') as file:
                                file.write(metrics_table)

                        gvis.visualise_performance(labels, predictions, probabilities)
                        roc_fig.savefig(f'report/{args.classifier}/{gen_model_name}_rocauc_{args.classifier}.png')
                        plt.figure(3).savefig(f'report/{args.classifier}/{gen_model_name}_confmatrix_{args.classifier}.png')
                        plt.close('all')  # Resets figure counter
        else:
                print(f'Training {gen_model_name} with {args.classifier}')
                predictions, probabilities = gclass.predict(gen_model_X, labels, classifier, parameters, 
                        confounds=confounds, return_probabilities=True, inner_cv_kwargs={'n_splits': 5})
                gclass.evaluate_performance(labels, predictions)

                # Plot classifier output
                gvis.visualise_performance(labels, predictions, probabilities)
                plt.show()
