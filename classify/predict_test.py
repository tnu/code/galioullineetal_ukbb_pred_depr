import sys
sys.path.insert(0, 'genbed-master')

from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.svm import SVC
from sklearn.linear_model import LinearRegression
from sklearn.metrics import balanced_accuracy_score
from sklearn.model_selection import permutation_test_score
from sklearn.model_selection import GridSearchCV

import pandas as pd
import numpy as np
import matplotlib, matplotlib.pyplot as plt

import genbed.classification as gclass
import genbed.statistics as gstats
import genbed.visualisation as gvis
import genbed.utilities as utils

# Set random seed for reproducibility
np.random.seed(42)

# Load features
X_train = pd.read_csv('../dcm/output/train/dummy_X_train.csv')
y_train = pd.read_csv('../dcm/output/train/dummy_y_train.csv', squeeze=True).astype('category') # squeeze=True

X_test = pd.read_csv('../dcm/output/test/dummy_X_test.csv')
y_test = pd.read_csv('../dcm/output/test/dummy_y_test.csv', squeeze=True).astype('category') # squeeze=True

# All functions allow the removal of a set of confounds via linear regression
train_confounds = pd.read_csv('../dataset/dummy_train_confounds.csv')
train_confounds['Gender'] = train_confounds['Gender'].astype('category').cat.codes
train_confounds['Handedness'] = train_confounds['Handedness'].astype('category').cat.codes
train_confounds['Smoking'] = train_confounds['Smoking'].astype('category').cat.codes
train_confounds['Cannabis'] = train_confounds['Cannabis'].astype('category').cat.codes
train_confounds['Drug Addiction'] = train_confounds['Drug Addiction'].astype('category').cat.codes
train_confounds['Alcohol'] = train_confounds['Alcohol'].astype('category').cat.codes

test_confounds = pd.read_csv('../dataset/dummy_test_confounds.csv')
test_confounds['Gender'] = test_confounds['Gender'].astype('category').cat.codes
test_confounds['Handedness'] = test_confounds['Handedness'].astype('category').cat.codes
test_confounds['Smoking'] = test_confounds['Smoking'].astype('category').cat.codes
test_confounds['Cannabis'] = test_confounds['Cannabis'].astype('category').cat.codes
test_confounds['Drug Addiction'] = test_confounds['Drug Addiction'].astype('category').cat.codes
test_confounds['Alcohol'] = test_confounds['Alcohol'].astype('category').cat.codes

classifier = SVC(probability=True)

X_train, y_train, train_confounds = utils.sanitise_inputs(X_train, y_train, train_confounds)
X_test, y_test, test_confounds = utils.sanitise_inputs(X_test, y_test, test_confounds)

# Normalise (respecting decision to remove means)
scaler  = StandardScaler(with_mean=True)
scaler.fit(train_confounds)
# `with_mean`: If True, center the data before scaling.
train_confounds = scaler.transform(train_confounds)
test_confounds  = scaler.transform(test_confounds)
# Remove confounds (but don't remove the data means)
regression = LinearRegression(fit_intercept=False)
regression.fit(train_confounds, X_train)
X_train = X_train - regression.predict(train_confounds)
X_test  = X_test  - regression.predict(test_confounds)
# Normalise data
scaler  = StandardScaler().fit(X_train)
X_train = scaler.transform(X_train)
X_test  = scaler.transform(X_test)

parameters = {
    'C': [0.01, 0.1, 1],
    'gamma': [1, 0.1, 0.01, 0.001],
    'kernel': ['sigmoid']
}

clf = SVC(probability=True)
search = GridSearchCV(clf, parameters, cv=5, n_jobs=-1, verbose=3)
search.fit(X_train, y_train)
clf = search.best_estimator_

pred_test = clf.predict(X_test)

acc = balanced_accuracy_score(y_test, pred_test)
print('Non-permuted Score:', acc)

n_perms = 100
perm_scores = []
for i in range(n_perms):
    perm_y_test = np.random.permutation(y_test)
    pred = clf.predict(X_test)
    perm_acc = balanced_accuracy_score(perm_y_test, pred)
    print(f'{i}:', perm_acc)
    perm_scores.append(perm_acc)

num_better_scores = np.count_nonzero(np.array(perm_scores) >= acc)
print('Number of better permuted scores:', num_better_scores)
pval = (num_better_scores + 1) / (n_perms + 1)
print('p-value:', pval)

fig, ax = plt.subplots()

ax.hist(perm_scores, bins=20, density=True)
ax.axvline(acc, ls='--', color='r')
score_label = (f"Score on original\ndata: {acc:.2f}\n"
               f"(p-value: {pval:.3f})")
ax.text(0.7, 260, score_label, fontsize=12)
ax.set_xlabel("Accuracy score")
_ = ax.set_ylabel("Number of tests")

plt.show()
