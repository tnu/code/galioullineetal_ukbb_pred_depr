# Predicting Depression

Our goal is to develop predictive models for depression using UK biobank data.

### Example Generation

To generate matched positive (subjects who developed depression) and negative examples (subjects who stayed healthy) from the main UKB dataset simply use the `create_depr_examples.py` script located in /dataset. All you need to give it is where your main UKB dataset is stored, and where you would like the program to output the examples. The script's running time is about 15 minutes.

Here's the program's help message:

```bash
usage: create_depr_examples.py [-h] [-main_data MAIN_DATA] [-out_dir OUT_DIR]

optional arguments:
  -h, --help            show this help message and exit
  -main_data MAIN_DATA  path to the main UKB dataset
  -out_dir OUT_DIR      path to the output directory for examples
```

Here's a usage example:

```bash
cd dataset
python create_depr_examples.py -main_data ukb_main_data/ukb_main_tabular_data.txt -out_dir examples
```

If running locally:

- The main dataset can be downloaded from UK Biobank directly as detailed in the [access guide](https://biobank.ctsu.ox.ac.uk/~bbdatan/Accessing_UKB_data_v2.3.pdf)

- Three extra libraries are used on top of the base Python 3 installation: matplotlib, tqdm, and sklearn. To install them use either of:

```shell
pip install matplotlib tqdm scikit-learn

conda env create --file environment.yml
conda activate ukbb_depr
```

After this process is complete, bulk files will be generated, which can be used to download the data using the standard UKB pipeline (with approval), for which documentation can be found [here](https://biobank.ndph.ox.ac.uk/ukb/ukb/docs/ukbfetch_instruct.html).

### Subject Selection

Patients consist of subjects who reported never having depression for a week in their first brain imaging questionnaire (2014+) and who...

- [Have since reported first depressive episode](http://biobank.ctsu.ox.ac.uk/crystal/field.cgi?id=130894) (clinical data): 5 subjects
- [Have since had a clinical depression-related encounter](http://biobank.ndph.ox.ac.uk/showcase/field.cgi?id=41270) (clinical data): 31 subjects
- [Have since reported being depressed for at least a week](http://biobank.ctsu.ox.ac.uk/crystal/field.cgi?id=4598) (2019+ imaging): 203 subjects
- [Have since reported being diagnosed with depression](http://biobank.ctsu.ox.ac.uk/crystal/field.cgi?id=20002) (2019+ imaging): 12 subjects

- [Have since been professionally diagnosed with depression](http://biobank.ctsu.ox.ac.uk/crystal/field.cgi?id=20544) (2016/17 MHQ): 90 subjects
- [Have since indicated probable lifetime depression according to the CIDI questionnaire](../wikis/Individual-Set-Definitions#using-cidi) (2016/17 MHQ): 165 subjects
- [Have since reported high PHQ scores](http://biobank.ndph.ox.ac.uk/showcase/field.cgi?id=2050) (2019+ imaging): 6 subjects
- [Have since taken anti-depressants](../wikis/Individual-Set-Definitions#using-drug-data) (2019+ imaging): 6 subjects

Taking the union of the above sets gives us 464 subjects who we now define as having "developed depression". 

Negative examples consist of subjects who reported never having been depressed for a week in their first brain imaging questionnaire (2014+) and reported the same in their second brain imaging questionnaire (2019+). This gives us 1076 subjects who we now define as having "stayed healthy".

The intersection of these two sets contains 9 subjects, who we exclude from both sets.

After attempting to match each patient to a control on gender, age, handedness, tobacco smoking frequency, alcohol consumption, illicit drug addiction, and cannabis consumption, we lose 2 patients, leaving us with 453 patients and 453 controls.

### Feature Set Generation

Once the data has been downloaded, resting-state time series for 25 ICA components can be extracted with the following command:

```bash
mkdir roi_25; for f in *.zip; do unzip -p $f fMRI/rfMRI_25.dr/dr_stage1.txt > "roi_25/rfmri_ts_${f%_20227_2_0.zip}.txt"; echo "Extracting from: $f"; done
```

The 100 ICA component version of the command looks like this:

```bash
mkdir roi_100; for f in *.zip; do unzip -p $f fMRI/rfMRI_100.dr/dr_stage1.txt > "roi_100/rfmri_ts_${f%_20227_2_0.zip}.txt"; echo "Extracting from: $f"; done
```

The timeseries in these folders will be used to generate feature sets based on DCM parameters using MATLAB scripts in the `dcm` folder of this repository.  After navigating to the `dcm` folder please execute the following steps:

- Download SPM12 using `wget https://www.fil.ion.ucl.ac.uk/spm/download/restricted/eldorado/spm12.zip`

- Unzip SPM12 with `unzip spm12.zip`

- Download Tapas with `wget https://github.com/translationalneuromodeling/tapas/releases/download/v4.0.0/tapas.zip `

- Unzip rDCM with `unzip tapas.zip 'tapas-master/rDCM/*' -d 'rDCM'`

- Use the MATLAB scripts in this repository to create the feature sets. For a 6 ICA component rDCM feature set on the training data, run the following command:

  ```bash
  matlab write_rdcm_features('train_controls/roi_25', 'train_patients/roi_25', 6)
  ```

  This should create two files in the output folder, namely the features X and the labels y.

### Classification

Now that the features and labels have been generated, we can run nested cross-validation to get an idea of classification performance. Navigate to the repository's `classify` folder and execute the following steps:

- Clone the genbed repository from https://github.com/translationalneuromodeling/tapas/tree/master/genbed

- Run `python classify.py` to execute with default parameters. For more advanced usage see the script's help description:

  ```bash
  usage: classify.py [-h] [-features FEATURES] [-classifier CLASSIFIER]
  
  optional arguments:
    -h, --help             show this help message and exit
    -features FEATURES     which feature set to use
    -classifier CLASSIFIER type of classifier to use
  ```

  This should result in accuracies of around 57% for a 6 IC rDCM, 61% for a 21 IC rDCM, and 63% for a 55 IC rDCM.
  