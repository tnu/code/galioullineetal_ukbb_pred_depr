import argparse
import random
import pandas as pd

import cidi_depr
import drug_depr
import clinical_report_depr
import phq
import inpatient_depr_diag

import match_patients_controls
import ukb_depr_utils as utils
import matplotlib.pyplot as plt

from pathlib import Path
from datetime import datetime
from sklearn.model_selection import train_test_split

def get_train_test_splits(matched_pairs, rfmri_map_subs, test_prop=0.2):
    # Subjects to keep in the training set (due to contrast mapping)
    map_pairs = {matched_pair for matched_pair in matched_pairs if matched_pair[0] in rfmri_map_subs or matched_pair[1] in rfmri_map_subs}
    # Number of subjects who should be in the test set according to ratio
    num_test_pairs = round(len(matched_pairs) * test_prop)
    # Subjects not used for contrast mapping, sorted for reproducibility
    without_map_pairs = sorted(list(matched_pairs - map_pairs))
    # Train test split excluding contrast mapping subjects
    without_map_subs_train, test = train_test_split(without_map_pairs, test_size=num_test_pairs, random_state=42)
    # Add contrast mapping subjects back into training set
    train = list(set(without_map_subs_train) | map_pairs)
    # Unpack into patient/control sets and sort for reproducibility
    train_patients = sorted([matched_pair[0] for matched_pair in train])
    test_patients = sorted([matched_pair[0] for matched_pair in test])
    train_controls = sorted([matched_pair[1] for matched_pair in train])
    test_controls = sorted([matched_pair[1] for matched_pair in test])
    print('Intersection of patient train/test sets:', len(set(train_patients) & set(test_patients)))
    print('Intersection of control train/test sets:', len(set(train_controls) & set(test_controls)))
    print('Intersection of patients and controls:', len((set(train_patients) | set(test_patients)) & (set(train_controls) | set(test_controls))))
    return train_patients, test_patients, train_controls, test_controls

def get_depr_date(eid, depr_dates):
    subject_dates = []
    for i, depr_date_dict in enumerate(depr_dates):
        date = depr_date_dict.get(eid, 'NA')
        print(date, i)
        if not date == 'NA':
            # Antidepressant info has slightly different format
            if type(date) == tuple:
                subject_dates.append(datetime.strptime(date[0], '%Y-%m-%dT%H:%M:%S'))
            else:
                subject_dates.append(date)
    # print(subject_dates, eid)
    subject_dates.sort(reverse=True)
    subject_date = subject_dates[0]
    return subject_date

def plot_baseline_depr_diff(examples, never_depr_week_14, depr_dates):
    baseline_dates = [never_depr_week_14[eid] for eid in examples]
    depr_dates = [get_depr_date(eid, depr_dates) for eid in examples]
    date_diffs = [depr_date - baseline_date for depr_date, baseline_date in zip(depr_dates, baseline_dates)]
    date_diffs_days = [date_diff.days for date_diff in date_diffs]
    fig, ax = plt.subplots()
    plt.title('Time difference between baseline and depressive datapoint')
    ax.hist(date_diffs_days, bins=50)
    plt.xlabel('Days')
    plt.ylabel('Number of subjects')
    plt.show()

def get_patients(never_depr_week_14, fmri_dataset_path, temp_dir, plot=False):
    patient_defs = []

    # Professionally diagnosed (MHQ)
    prof_diag_start_idx = 11591  #f20544-0.1
    prof_diag_end_idx = 11606  #f20544-0.16
    prof_diag_end_idxs = range(prof_diag_start_idx, prof_diag_end_idx + 1)
    depression_diagnosis_code = '11'
    prof_diag_depr_mhq_pkl = temp_dir / 'prof_diag_mhq_depr.pkl'
    print('\n[1/8] Retrieving subjects who were diagnosed with depression some time before 2016 MHQ')
    prof_diag_depr_mhq = utils.get_field_data(prof_diag_end_idxs, depression_diagnosis_code, utils.mhq_date_taken_idx, fmri_dataset_path, prof_diag_depr_mhq_pkl)
    dev_depr_prof_diag = utils.get_keys_with_dates_between_dicts(never_depr_week_14, prof_diag_depr_mhq)
    patient_defs.append(dev_depr_prof_diag)
    print('...and diagnosed with depression some time before 2016 MHQ:', len(dev_depr_prof_diag))

    # Probable lifetime depression according to CIDI (MHQ)
    print('\n[2/8] Retrieving subjects who have probable lifetime depression in 2016 MHQ')
    cidi_depr_mhq_pkl = temp_dir / 'cidi_depr_mhq.pkl'
    cidi_depr_mhq = cidi_depr.get_cidi_depr_mhq(fmri_dataset_path, cidi_depr_mhq_pkl)
    dev_depr_cidi = utils.get_keys_with_dates_between_dicts(never_depr_week_14, cidi_depr_mhq)
    patient_defs.append(dev_depr_cidi)
    print('...and has probable lifetime depression in 2016 MHQ:', len(dev_depr_cidi))

    # Anti-depressants (2019+)
    antidepr_subjects_19_pkl = temp_dir / 'antidepr_subjects_19.pkl'
    print('\n[3/8] Retrieving subjects who take anti-depressants in 2019+')
    antidepr_subjects_19 = drug_depr.get_antidepr_subjects(fmri_dataset_path, antidepr_subjects_19_pkl, instance='2019')
    dev_depr_antidepr = never_depr_week_14.keys() & antidepr_subjects_19.keys()
    patient_defs.append(dev_depr_antidepr)
    print('...and takes anti-depressants in 2019+:', len(dev_depr_antidepr))

    # Depressed for a week (2019+)
    depr_week_19_idx = 4101  #f4598-3.0
    depr_week_19_pickle = temp_dir / 'depr_week_19.pkl'
    print('\n[4/8] Retrieving subjects who have been depressed for a week according to data point in 2019+')
    depr_week_19 = utils.get_field_data([depr_week_19_idx], '1', utils.fmri_date_19_idx, fmri_dataset_path, depr_week_19_pickle)
    dev_depr_week = never_depr_week_14.keys() & depr_week_19.keys()
    patient_defs.append(dev_depr_week)
    print('...and depressed for at least a week before 2019+:', len(dev_depr_week))

    # Reported depression diagnosis (2019+)
    reported_depr_diag_19_start_idx = 8481  #f20002-3.0
    reported_depr_diag_19_end_idx = 8514  #f20002-3.33
    reported_depr_diag_19_idxs = range(reported_depr_diag_19_start_idx, reported_depr_diag_19_end_idx + 1)
    reported_depr_diag_19_pkl = temp_dir / 'reported_depr_diag_19.pkl'
    print('\n[5/8] Retrieving subjects who reported being diagnosed with depression according to data point in 2019+')
    reported_depr_diag_19 = utils.get_field_data(reported_depr_diag_19_idxs, '1286', utils.fmri_date_19_idx, 
                                                                    fmri_dataset_path, reported_depr_diag_19_pkl)
    dev_depr_diag = never_depr_week_14.keys() & reported_depr_diag_19.keys()
    patient_defs.append(dev_depr_diag)
    print('...and reported being diagnosed with depression according to data point in 2019+:', len(dev_depr_diag))

    # First depressive episode (clinical)
    first_rep_depr_ep_idx = 23798  #f130894
    first_rep_depr_ep_pkl = temp_dir / 'first_rep_depr_ep.pkl'
    print('\n[6/8] Retrieving subjects who clinically reported their first episode of depression')
    first_rep_depr_ep = clinical_report_depr.get_first_report_depr_ep(fmri_dataset_path, first_rep_depr_ep_pkl)
    dev_depr_first_ep = utils.get_keys_with_dates_between_dicts(never_depr_week_14, first_rep_depr_ep)
    patient_defs.append(dev_depr_first_ep)
    print('...and reported first depressive episode some time after:', len(dev_depr_first_ep))

    # High PHQ Scores
    high_phq_score_pkl = temp_dir / 'high_phq_score.pkl'
    print('\n[7/8] Retrieving subjects who have high PHQ scores')
    high_phq_score = phq.get_high_phq_scores(fmri_dataset_path, 5, high_phq_score_pkl, '2019')
    dev_depr_phq = never_depr_week_14.keys() & high_phq_score.keys()
    patient_defs.append(dev_depr_phq)
    print('...and had high PHQ scores in 2019+:', len(dev_depr_phq))

    # Depression-related encounter according to in-patient hospital data (clinical)
    inpatient_depr_diag_pkl = temp_dir / 'inpatient_depr_diag.pkl'
    print('\n[8/8] Retrieving subjects had a clinical depression-related encounter')
    inpatient_depr_diag_subs = inpatient_depr_diag.get_inpatient_depr_diag(inpatient_depr_diag_pkl)
    dev_depr_inpatient = utils.get_keys_with_dates_between_dicts(never_depr_week_14, inpatient_depr_diag_subs)
    patient_defs.append(dev_depr_inpatient)
    print('...and had a clinical depression-related encounter since:', len(dev_depr_inpatient))

    patients = set.union(*patient_defs)
    # Plots duration between healthy and depressed datapoints
    if plot:
        depr_dates = [prof_diag_depr_mhq, cidi_depr_mhq, antidepr_subjects_19, depr_week_19, 
                        reported_depr_diag_19, first_rep_depr_ep, high_phq_score, inpatient_depr_diag_subs]
        plot_baseline_depr_diff(patient_defs, never_depr_week_14, depr_dates)

    return patients

def create_examples(main_data, out_dir, plot_confounds, export_confounds):
    temp_dir = out_dir / 'temp'
    temp_dir.mkdir(exist_ok=True)
    fmri_dataset_path = temp_dir / 'fmri_subjects.txt'
    print('Writing fMRI subjects to file')
    utils.write_fmri_subjects_to_file(main_data, fmri_dataset_path)
    print('Done writing fMRI subjects to file')

    # Baseline
    ever_depr_week_14_idx = 4100  #f4598-2.0
    never_depr_week_14_pkl = temp_dir / 'never_depr_week_14.pkl'
    print('\nRetrieving subjects who have never been depressed for a week according to data point in 2014+')
    never_depr_week_14 = utils.get_field_data([ever_depr_week_14_idx], '0', utils.fmri_date_14_idx, fmri_dataset_path, never_depr_week_14_pkl)
    print('Never depressed for a week according to data point in 2014+:', len(never_depr_week_14))

    patients = get_patients(never_depr_week_14, fmri_dataset_path, temp_dir)
    print('Patients before matching:', len(patients))

    # Definition for controls
    ever_depr_week_19_idx = 4101  #f4598-3.0
    ever_depr_week_19_pkl = temp_dir / 'ever_depr_week_19.pkl'
    print('\nRetrieving subjects who were never depressed for at least a week according to data point in 2019+')
    never_depr_week_19 = utils.get_field_data([ever_depr_week_19_idx], '0', utils.fmri_date_19_idx, fmri_dataset_path, ever_depr_week_19_pkl)
    print('Never depressed for at least a week according to data point in 2019+:', len(never_depr_week_19))

    # Controls
    controls = never_depr_week_14.keys() & never_depr_week_19.keys()
    print('Controls before matching:', len(controls))

    # Remove subjects who show up in both patients and controls
    print('\nNumber of subjects appearing as both patients and controls:', len(patients & controls))
    patients -= patients & controls
    controls -= patients & controls

    matched_pairs, matched_patient_demo_data, matched_control_demo_data = \
        match_patients_controls.match(patients, controls, fmri_dataset_path, temp_dir)
    print('Number of matched patient/control pairs:', len(matched_pairs))

    # Keep subjects who were used for fmri mapping in the training set
    rfmri_map_subs_pkl = temp_dir / 'rest_fmri_map_subjects.pkl'
    print('\nKeeping subjects who were used for fmri mapping in the training set')
    rfmri_map_subs = utils.get_first_n_subjects(4181, utils.fmri_date_14_idx, fmri_dataset_path, rfmri_map_subs_pkl)
    
    train_patients, test_patients, train_controls, test_controls = get_train_test_splits(matched_pairs, rfmri_map_subs)
    print('Patients training set size:', len(train_patients), '| Patients hold-out set size:', len(test_patients))
    print('Controls training set size:', len(train_controls), '| Controls hold-out set size:', len(test_controls))

    if plot_confounds or export_confounds:
        matched_train_patient_demo_data = \
            {eid: demo_data for eid, demo_data in matched_patient_demo_data.items() if eid in train_patients}
        matched_train_control_demo_data = \
            {eid: demo_data for eid, demo_data in matched_control_demo_data.items() if eid in train_controls}
        matched_test_patient_demo_data = \
                {eid: demo_data for eid, demo_data in matched_patient_demo_data.items() if eid in test_patients}
        matched_test_control_demo_data = \
            {eid: demo_data for eid, demo_data in matched_control_demo_data.items() if eid in test_controls}
        if plot_confounds:
            match_patients_controls.plot_demo_data(matched_train_patient_demo_data, 
                                                matched_train_control_demo_data, 'Training Set Confound Data')
            match_patients_controls.plot_demo_data(matched_test_patient_demo_data, 
                                                matched_test_control_demo_data, 'Hold-Out Set Confound Data')
            plt.show()
        if export_confounds:
            train_control_confounds = [(control, ) + matched_train_control_demo_data[control] for control in train_controls]
            train_control_confounds_df = pd.DataFrame(train_control_confounds, 
                    columns=['EID', 'Gender', 'Age', 'Handedness', 'Smoking', 'Cannabis', 'Drug Addiction', 'Alcohol'])
            train_patient_confounds = [(patient, ) + matched_train_patient_demo_data[patient] for patient in train_patients]
            train_patient_confounds_df = pd.DataFrame(train_patient_confounds, 
                    columns=['EID', 'Gender', 'Age', 'Handedness', 'Smoking', 'Cannabis', 'Drug Addiction', 'Alcohol'])
            train_confounds_df = train_control_confounds_df.append(train_patient_confounds_df, ignore_index=True)
            train_confounds_df.to_csv('train_confounds.csv', index=False)

            test_control_confounds = [(control, ) + matched_test_control_demo_data[control] for control in test_controls]
            test_control_confounds_df = pd.DataFrame(test_control_confounds, 
                    columns=['EID', 'Gender', 'Age', 'Handedness', 'Smoking', 'Cannabis', 'Drug Addiction', 'Alcohol'])
            test_patient_confounds = [(patient, ) + matched_test_patient_demo_data[patient] for patient in test_patients]
            test_patient_confounds_df = pd.DataFrame(test_patient_confounds, 
                    columns=['EID', 'Gender', 'Age', 'Handedness', 'Smoking', 'Cannabis', 'Drug Addiction', 'Alcohol'])
            test_confounds_df = test_control_confounds_df.append(test_patient_confounds_df, ignore_index=True)
            test_confounds_df.to_csv('test_confounds.csv', index=False)
    
    # Create resting-state fMRI bulk file
    rfmri_field_name = '20227_2_0'
    print('\nWriting patients to file (rfmri)')
    utils.write_bulk_file(train_patients, rfmri_field_name, out_dir / 'rfmri_train_patients.bulk')
    utils.write_bulk_file(test_patients, rfmri_field_name, out_dir / 'rfmri_test_patients.bulk')
    print('\nWriting controls to file (rfmri)')
    utils.write_bulk_file(train_controls, rfmri_field_name, out_dir / 'rfmri_train_controls.bulk')
    utils.write_bulk_file(test_controls, rfmri_field_name, out_dir / 'rfmri_test_controls.bulk')

    # Create task fMRI bulk file
    tfmri_field_name = '20249_2_0'
    print('\nWriting patients to file (tfmri)')
    utils.write_bulk_file(train_patients, tfmri_field_name, out_dir / 'tfmri_train_patients.bulk')
    utils.write_bulk_file(test_patients, tfmri_field_name, out_dir / 'tfmri_test_patients.bulk')
    print('\nWriting controls to file (tfmri)')
    utils.write_bulk_file(train_controls, tfmri_field_name, out_dir / 'tfmri_train_controls.bulk')
    utils.write_bulk_file(test_controls, tfmri_field_name, out_dir / 'tfmri_test_controls.bulk')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-main_data',
        type=Path,
        default='./ukb_main_data/ukb_main_tabular_data.txt',
        help="path to the main UKB dataset",
    )
    parser.add_argument(
        '-out_dir',
        type=Path,
        default='./examples',
        help="path to the output directory for examples",
    )
    parser.add_argument(
        '-plot_confounds',
        type=bool,
        default=False,
        help="plots confounds to compare training and hold-out set",
    )
    parser.add_argument(
        '-export_confounds',
        type=bool,
        default=True,
        help="exports confounds for regressing them out downstream",
    )
    args = parser.parse_args()
    if not args.main_data.is_file():
        raise FileNotFoundError(f'-main_data: "{args.main_data}"')
    if not args.out_dir.is_dir():
        args.out_dir.mkdir(exist_ok=True)
    create_examples(args.main_data, args.out_dir, args.plot_confounds, args.export_confounds)
