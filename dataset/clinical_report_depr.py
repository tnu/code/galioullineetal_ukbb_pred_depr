import os
import datetime
import ukb_depr_utils

from pathlib import Path
from tqdm import tqdm

def get_first_report_depr_ep(dataset_path, pickle_path):
    first_report_depressive_episode_idx = 23798  #f130894
    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    first_report_depr_ep = {}
    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as main_dataset_file:
            # Skip header
            next(main_dataset_file)
            for i, line in enumerate(main_dataset_file):
                pbar.update(len(line))
                subject = line.lstrip().split('\t')
                first_report_depr_ep_date_str = subject[first_report_depressive_episode_idx]
                eid = subject[0]
                if first_report_depr_ep_date_str:
                    first_report_depr_ep_date = datetime.datetime.strptime(first_report_depr_ep_date_str, '%Y-%m-%d')
                    first_report_depr_ep[eid] = first_report_depr_ep_date
    
    ukb_depr_utils.save_to_pickle(first_report_depr_ep, pickle_path)
    return first_report_depr_ep

def get_first_report_recurr_depr(main_dataset_path, pickle_path):
    first_report_recurr_depr_idx = 23800  #f130896
    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    first_report_recurr_depr = {}
    with open(main_dataset_path) as main_dataset_file:
        # Skip header
        next(main_dataset_file)
        for i, line in enumerate(main_dataset_file):
            if i % 10000 == 0:
                print(i)
            subject = line.lstrip().split('\t')
            first_report_recurr_depr_date_str = subject[first_report_recurr_depr_idx]
            eid = subject[0]
            if first_report_recurr_depr_date_str:
                first_report_depr_ep_date = datetime.datetime.strptime(first_report_recurr_depr_date_str, '%Y-%m-%d')
                first_report_recurr_depr[eid] = first_report_depr_ep_date
    
    ukb_depr_utils.save_to_pickle(first_report_recurr_depr, pickle_path)
    return first_report_recurr_depr