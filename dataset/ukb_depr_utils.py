import pickle
import datetime
import os

import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from operator import itemgetter
from pathlib import Path
from tqdm import tqdm

mhq_date_taken_idx = 11459  #f20400
base_bipolar_depr_status_idx = 10770  #f20126
main_data_lines = 502505  # Length of main dataset file

# fMRI dates
fmri_date_14_idx = 11992  #f21862-2.0
fmri_date_19_idx = fmri_date_14_idx + 1  #f21862-3.0

# fMRI files
fmri_14_rest_dicom_idx = 11192  #f20225-2.0
fmri_14_rest_nifti_idx = 11194  #f20227-2.0
fmri_14_rest_ica_idx = 15831  #f25754-2.0

fmri_14_task_dicom_idx = 11184  #f20217-2.0
fmri_14_task_nifti_idx = 11437  #f20249-2.0
fmri_14_task_contrast_idx = 15851 #f25765-2.0

def load_from_pickle(pickle_path):
    with open(pickle_path, 'rb') as pickle_file:
        return pickle.load(pickle_file)

def save_to_pickle(variable, pickle_path):
    with open(pickle_path, 'wb') as pickle_file:
        pickle.dump(variable, pickle_file)

def get_base_depr(dataset_path, pickle_path):
    if Path(pickle_path).is_file():
        return load_from_pickle(pickle_path)
    
    base_pos_depr = {}
    base_neg_depr = {}
    with open(dataset_path) as dataset_file:
        next(dataset_file)
        for i, line in enumerate(dataset_file):
            if i % 10000 == 0:
                print(i)
            subject = line.lstrip().split('\t')
            eid = subject[0]
            date_str = subject[11952]
            depr_status = subject[base_bipolar_depr_status_idx]
            # 0 means no bipolar or depression (~30k people)
            # 1 means bipolar 1 disorder (~800 people)
            # 2 means bipolar 2 disorder (~800 people)
            # 3 means probable recurrent major depression (severe) ~9k people
            # 4 means probable recurrent major depression (moderate) ~15k people
            # 5 means single Probable major depression episode ~8k
            if depr_status == '0':
                date = datetime.datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
                base_neg_depr[eid] = date
            elif depr_status in {'3', '4', '5'}:
                date = datetime.datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
                base_pos_depr[eid] = date

    save_to_pickle([base_pos_depr, base_neg_depr], pickle_path)
    return base_pos_depr, base_neg_depr

def get_brain_imaged(dataset_path, pickle_path, instance='2014'):
    brain_imaged_date_2019_idx = fmri_date_14_idx + 1
    if Path(pickle_path).is_file():
        return load_from_pickle(pickle_path)
    
    brain_imaged = {}
    with open(dataset_path) as dataset_file:
        next(dataset_file)
        for i, line in enumerate(dataset_file):
            if i % 10000 == 0:
                print(i)
            subject = line.lstrip().split('\t')
            # Date brain MRI was taken
            if instance == '2014':
                brain_imaged_date = subject[fmri_date_14_idx]
            else:
                brain_imaged_date = subject[brain_imaged_date_2019_idx]

            eid = subject[0]
            if brain_imaged_date:
                brain_imaged[eid] = datetime.datetime.strptime(brain_imaged_date, '%Y-%m-%dT%H:%M:%S')
                        
    save_to_pickle(brain_imaged, pickle_path)
    return brain_imaged

def get_keys_with_dates_between_dicts(dict1, dict2, exclusion_threshold_days=50):
    keys_with_dates_before = set()
    shared_keys = dict1.keys() & dict2.keys()
    for subject in shared_keys:
        date1 = dict1[subject]
        date2 = dict2[subject]
        if date1 < date2 and (date2 - date1).days >= exclusion_threshold_days:
            keys_with_dates_before.add(subject)
    return keys_with_dates_before

def plot_brain_imaging_dates(brain_imaged, interest_set):
    dates = []
    for subject in interest_set:
        dates.append(brain_imaged[subject])
    dates = mdates.date2num(dates)
    fig, ax = plt.subplots(1, 1)
    ax.hist(dates, bins=15, color='lightblue', edgecolor='black')
    locator = mdates.AutoDateLocator()
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(mdates.AutoDateFormatter(locator))
    plt.xlabel('Date')
    plt.ylabel('Number of Subjects')
    plt.title('MRI Scan Dates')
    plt.show()

def write_subset_to_file(subjects, dataset_path, output_path, overwrite=False):
    if Path(output_path).is_file() and not overwrite:
        return 'Output file already exists'

    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as dataset_file:
            with open(output_path, 'w') as output_file:
                for i, line in enumerate(dataset_file):
                    pbar.update(len(line))
                    subject = line.lstrip().split('\t')
                    eid = subject[0]
                    if eid in subjects:
                        output_file.write('\t'.join(subject))

def write_fmri_subjects_to_file(main_data, fmri_path, overwrite=False):
    if Path(fmri_path).is_file() and not overwrite:
        return 'Output file already exists'

    with tqdm(total=os.path.getsize(main_data)) as pbar:
        with open(main_data, encoding='utf-8', errors='ignore') as dataset_file:
            with open(fmri_path, 'w') as fmri_file:
                for i, line in enumerate(dataset_file):
                    pbar.update(len(line))
                    subject = line.lstrip().split('\t')
                    has_fmri_14_rest_ica = bool(subject[fmri_14_rest_ica_idx])
                    has_fmri_14_task_nifti = bool(subject[fmri_14_task_nifti_idx])
                    has_fmri_14_available = has_fmri_14_rest_ica and has_fmri_14_task_nifti
                    if has_fmri_14_available:
                        fmri_file.write('\t'.join(subject))

def get_field_data(target_field_idxs, target_response, date_idx, dataset_path, pickle_path='', overwrite=False):
    if Path(pickle_path).is_file() and not overwrite:
        return load_from_pickle(pickle_path)

    result = {}
    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as dataset_file:
            next(dataset_file)
            for i, line in enumerate(dataset_file):
                pbar.update(len(line))
                subject = line.lstrip().split('\t')
                eid = subject[0]
                date_str = subject[date_idx]
                for idx in target_field_idxs:
                    if subject[idx] == target_response and date_str:
                        if len(date_str) == len('2000-05-12T10:19:45'):
                            date = datetime.datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
                        else:
                            date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
                        result[eid] = date
    if pickle_path:
        save_to_pickle(result, pickle_path)
    return result

def get_field_data_simple(dataset_path, target_field_idx=0, save_field_idxs=[0], pickle_path='./field_data.pkl', overwrite=False):
    if Path(pickle_path).is_file() and not overwrite:
        return load_from_pickle(pickle_path)

    subjects = []
    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as dataset_file:
            next(dataset_file)
            for i, line in enumerate(dataset_file):
                pbar.update(len(line))
                subject = line.lstrip().split('\t')
                if subject[target_field_idx]:
                    subject_dict = {}
                    for save_field_idx in save_field_idxs:
                        subject_dict[save_field_idx] = subject[save_field_idx]
                    subjects.append(subject_dict)
    if pickle_path:
        save_to_pickle(subjects, pickle_path)
    return subjects

def get_first_n_subjects(first_n, date_idx, dataset_path, pickle_path='', overwrite=False):
    if Path(pickle_path).is_file() and not overwrite:
        return load_from_pickle(pickle_path)

    result = []
    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as dataset_file:
            next(dataset_file)
            for i, line in enumerate(dataset_file):
                pbar.update(len(line))
                subject = line.lstrip().split('\t')
                eid = subject[0]
                date_str = subject[date_idx]
                if date_str:
                    if len(date_str) == len('2000-05-12T10:19:45'):
                        date = datetime.datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
                    else:
                        date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
                    result.append((eid, date))

    result = sorted(result, key=itemgetter(1))[:first_n]
    result = {eid_date[0] for eid_date in result}

    if pickle_path:
        save_to_pickle(result, pickle_path)
    return result

def write_bulk_file(subjects, field_name, output_path, overwrite=False):
    if Path(output_path).is_file() and not overwrite:
        return 'Output file already exists'

    with open(output_path, 'w') as output_file:
        for subject in subjects:
            line = subject + ' ' + field_name + '\n'
            output_file.write(line)
