import os
import datetime
import ukb_depr_utils as utils

from pathlib import Path
from tqdm import tqdm
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

# demographic info of interest
sex_idx = 26  #f31
birth_year_idx = 27  #f34
handedness_idx = 1089  #f1707
smoking_idx = 927  #f1249-2.0
cannabis_idx = 11505  #f20453-0.0
drug_idx = 11509  #f20457-0.0
alcohol_idx = 1047  #f1558-2.0

def get_demo_data(examples, dataset_path, pickle_path='', overwrite=False):
    if Path(pickle_path).is_file() and not overwrite:
        return utils.load_from_pickle(pickle_path)

    demo_data = {}
    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as dataset_file:
            for i, line in enumerate(dataset_file):
                pbar.update(len(line))
                subject = line.lstrip().split('\t')
                eid = subject[0]
                if eid not in examples:
                    continue
                sex_code = subject[sex_idx]
                sex = 'Female' if sex_code == '0' else 'Male'
                age_str = subject[birth_year_idx]
                birth_year = datetime.datetime.strptime(age_str, '%Y').year
                fmri_date_14_str = subject[utils.fmri_date_14_idx]
                fmri_year = datetime.datetime.strptime(fmri_date_14_str, '%Y-%m-%dT%H:%M:%S').year
                age = fmri_year - birth_year

                handedness_code = subject[handedness_idx]
                if handedness_code == '1':
                    handedness = 'Right'
                elif handedness_code == '2':
                    handedness = 'Left'
                elif handedness_code == '3':
                    handedness = 'Ambidextrous'
                else:
                    handedness = 'NA'

                smoke_code = subject[smoking_idx]
                if smoke_code == '1':
                    smoking = 'Frequent'
                elif smoke_code == '2':
                    smoking = 'Sometimes'
                elif smoke_code in {'3', '4'}:
                    smoking = 'Never'
                else:
                    smoking = 'NA'
                
                cannabis_code = subject[cannabis_idx]
                if cannabis_code in {'', '0', '1'}:
                    cannabis = 'No'
                elif cannabis_code in {'2', '3', '4'}:
                    cannabis = 'Yes'
                else:
                    cannabis = 'NA'

                drug_addict_code = subject[drug_idx]
                if drug_addict_code in {'', '0'}:
                    drug_addict = 'No'
                elif drug_addict_code == '1':
                    drug_addict = 'Yes'
                else:
                    drug_addict = 'NA'

                alcohol_code = subject[alcohol_idx]
                if alcohol_code in {'1', '2'}:
                    alcohol = 'Frequent'
                elif alcohol_code in {'3', '4'}:
                    alcohol = 'Sometimes'
                elif alcohol_code in {'5', '6'}:
                    alcohol = 'Never'
                else:
                    alcohol = 'NA'
                demo_data[eid] = (sex, age, handedness, smoking, cannabis, drug_addict, alcohol)
    if pickle_path:
        utils.save_to_pickle(demo_data, pickle_path)
    return demo_data

def plot_demo_data(patient_demo_data, control_demo_data, title):
    fig = plt.figure()
    fig.suptitle(title)

    gs = gridspec.GridSpec(4, 8)
    sex_ax = plt.subplot(gs[:2, :2])
    age_ax = plt.subplot(gs[:2, 2:4])
    hand_ax = plt.subplot(gs[:2, 4:6])
    smoke_ax = plt.subplot(gs[:2, 6:8])
    cannabis_ax = plt.subplot(gs[2:4, 1:3])
    drug_ax = plt.subplot(gs[2:4, 3:5])
    alcohol_ax = plt.subplot(gs[2:4, 5:7])
    
    fig.tight_layout()
    fig.subplots_adjust(top=0.9)

    # Sex
    pos_sex_data = [datapoint[0] for datapoint in patient_demo_data.values()]
    neg_sex_data = [datapoint[0] for datapoint in control_demo_data.values()]

    sex_ax.hist([pos_sex_data, neg_sex_data], label=['Patient', 'Control'])
    sex_ax.set_title('Sex Distribution')
    sex_ax.set_ylabel('Number of Subjects')
    sex_ax.legend()

    # Age
    pos_age_data = [datapoint[1] for datapoint in patient_demo_data.values()]
    neg_age_data = [datapoint[1] for datapoint in control_demo_data.values()]
    age_ax.hist([pos_age_data, neg_age_data], label=['Patient', 'Control'])
    age_ax.set_title('Age Distribution')
    age_ax.set_ylabel('Number of Subjects')
    age_ax.legend()

    # Handedness
    pos_hand_data = ['Right', 'Left', 'Ambidextrous', 'NA'] + [datapoint[2] for datapoint in patient_demo_data.values()]
    neg_hand_data = [datapoint[2] for datapoint in control_demo_data.values()]
    hand_ax.hist([pos_hand_data, neg_hand_data], label=['Patient', 'Control'])
    hand_ax.set_title('Handedness Distribution')
    hand_ax.set_ylabel('Number of Subjects')
    hand_ax.set_xticklabels(['Right', 'Left', 'Ambidextrous', 'NA'])
    hand_ax.legend()

    # Smoking
    pos_smoke_data = ['Frequent', 'Sometimes', 'Never'] + [datapoint[3] for datapoint in patient_demo_data.values()]
    neg_smoke_data = [datapoint[3] for datapoint in control_demo_data.values()]
    smoke_ax.hist([pos_smoke_data, neg_smoke_data], label=['Patient', 'Control'])
    smoke_ax.set_title('Past Smoking Frequency')
    smoke_ax.set_ylabel('Number of Subjects')
    smoke_ax.legend()

    # Cannabis
    pos_cannabis_data = [datapoint[4] for datapoint in patient_demo_data.values()]
    neg_cannabis_data = [datapoint[4] for datapoint in control_demo_data.values()]
    cannabis_ax.hist([pos_cannabis_data, neg_cannabis_data], label=['Patient', 'Control'])
    cannabis_ax.set_title('Past Cannabis Use Frequency')
    cannabis_ax.set_ylabel('Number of Subjects')
    cannabis_ax.legend()

    # Drug Addiction
    pos_drug_data = [datapoint[5] for datapoint in patient_demo_data.values()]
    neg_drug_data = [datapoint[5] for datapoint in control_demo_data.values()]
    drug_ax.hist([pos_drug_data, neg_drug_data], label=['Patient', 'Control'])
    drug_ax.set_title('Ongoing Drug Addiction')
    drug_ax.set_ylabel('Number of Subjects')
    drug_ax.legend()

    # Alcohol
    pos_alcohol_data = ['Frequent', 'Sometimes', 'Never'] + [datapoint[6] for datapoint in patient_demo_data.values()]
    neg_alcohol_data = [datapoint[6] for datapoint in control_demo_data.values()]
    alcohol_ax.hist([pos_alcohol_data, neg_alcohol_data], label=['Patient', 'Control'])
    alcohol_ax.set_title('Alcohol Use')
    alcohol_ax.set_ylabel('Number of Subjects')
    alcohol_ax.legend()

def match_distr(patient_demo_data, control_demo_data):
    len_pos_demo_data = len(patient_demo_data)
    matched_pairs = set()
    matched_patient_demo_data = {}
    matched_control_demo_data = {}
    for patient_eid, patient_demo_datapoints in patient_demo_data.items():
        for control_eid, control_demo_datapoints in control_demo_data.items():
            matches = [patient_data == control_data for patient_data, control_data in zip(patient_demo_datapoints, control_demo_datapoints)]

            patient_age = patient_demo_datapoints[1]
            control_age = control_demo_datapoints[1]
            matches[1] = control_age - 5 <= patient_age <= control_age + 5

            if all(matches):
                matched_patient_demo_data[patient_eid] = patient_demo_datapoints
                matched_control_demo_data[control_eid] = control_demo_datapoints
                matched_pairs.add((patient_eid, control_eid))
                del control_demo_data[control_eid]
                break

    # Slightly relax condition for unmatched subjects
    unmatched_patient_demo_data = {eid: patient_demo_data for eid, patient_demo_data in patient_demo_data.items() if eid not in matched_patient_demo_data}
    for patient_eid, patient_demo_datapoints in unmatched_patient_demo_data.items():
        for control_eid, control_demo_datapoints in control_demo_data.items():
            matches = [patient_data == control_data for patient_data, control_data in zip(patient_demo_datapoints, control_demo_datapoints)]

            patient_age = patient_demo_datapoints[1]
            control_age = control_demo_datapoints[1]
            matches[1] = control_age - 5 <= patient_age <= control_age + 5

            if matches.count(False) <= 1:
                matched_patient_demo_data[patient_eid] = patient_demo_datapoints
                matched_control_demo_data[control_eid] = control_demo_datapoints
                matched_pairs.add((patient_eid, control_eid))
                del control_demo_data[control_eid]
                break
    
    print('Lost', len_pos_demo_data - len(matched_patient_demo_data), 'patients due to matching')
    return matched_pairs, matched_patient_demo_data, matched_control_demo_data

def match(pos_examples, neg_examples, dataset_path, temp_dir, overwrite=False, plot=False):
    print('\nRetrieving patient demographic data')
    patient_pkl = temp_dir / 'pos_demo.pkl'
    patient_demo_data = get_demo_data(pos_examples, dataset_path, patient_pkl, overwrite)
    print('\nRetrieving control demographic data')
    control_demo_pkl = temp_dir / 'neg_demo.pkl'
    control_demo_data = get_demo_data(neg_examples, dataset_path, control_demo_pkl, overwrite)

    matched_pairs, matched_patient_demo_data, matched_control_demo_data = match_distr(patient_demo_data, control_demo_data)
    if plot:
        plot_demo_data(patient_demo_data, control_demo_data, 'Unmatched Data')
        plot_demo_data(matched_patient_demo_data, matched_control_demo_data, 'Matched Data')
        plt.show()
    return matched_pairs, matched_patient_demo_data, matched_control_demo_data
