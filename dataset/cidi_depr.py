import os
import datetime
import ukb_depr_utils

from tqdm import tqdm
from pathlib import Path

def get_cidi_depr_mhq(dataset_path, pickle_path=''):
    prolonged_feelings_depression_idx = 11500
    prolonged_loss_interest_idx = 11497
    feeling_tiredeness_idx = 11503
    weight_change_idx = 11583
    sleep_change_idx = 11579
    difficulty_concentrating_idx = 11491
    feeling_worthless_idx = 11504
    thoughts_of_death_idx = 11493
    affected_most_of_the_day_idx = 11492
    depressed_almost_every_day_idx = 11495
    impact_on_normal_roles_idx = 11496

    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    cidi_depr_mhq = {}
    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as dataset_file:
            next(dataset_file)
            for i, line in enumerate(dataset_file):
                pbar.update(len(line))
                subject = line.split('\t')
                prolonged_feelings_depression = subject[prolonged_feelings_depression_idx] == '1'
                prolonged_loss_interest = subject[prolonged_loss_interest_idx] == '1'
                feeling_tiredeness = subject[feeling_tiredeness_idx] == '1'
                weight_change = subject[weight_change_idx] in {'1', '2', '3'}
                sleep_change = subject[sleep_change_idx] == '1'
                difficulty_concentrating = subject[difficulty_concentrating_idx] == '1'
                feeling_worthless = subject[feeling_worthless_idx] == '1'
                thoughts_of_death = subject[thoughts_of_death_idx] == '1'
                affected_most_of_the_day = subject[affected_most_of_the_day_idx] in {'3', '4'}
                depressed_almost_every_day = subject[depressed_almost_every_day_idx] in {'2', '3'}
                impact_on_normal_roles = subject[impact_on_normal_roles_idx] in {'2', '3'}
                if (prolonged_feelings_depression and prolonged_loss_interest and
                    sum([prolonged_feelings_depression, prolonged_loss_interest, feeling_tiredeness, 
                        weight_change, sleep_change, difficulty_concentrating, feeling_worthless, thoughts_of_death]) and
                            affected_most_of_the_day and depressed_almost_every_day and impact_on_normal_roles):
                    eid = subject[0]
                    date_str = subject[ukb_depr_utils.mhq_date_taken_idx]
                    date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
                    cidi_depr_mhq[eid] = date
    if pickle_path:
        ukb_depr_utils.save_to_pickle(cidi_depr_mhq, pickle_path)
    return cidi_depr_mhq