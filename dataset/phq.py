import os
import ukb_depr_utils as utils

from tqdm import tqdm
from pathlib import Path
from datetime import datetime

def get_total_phq_score(dataset_path, pickle_path, instance='2014', overwrite=False):
    if Path(pickle_path).is_file() and not overwrite:
            return utils.load_from_pickle(pickle_path)
    
    if instance == '2014':
        depr_idx = 1196  #f2050-2.0
        disinterest_idx = 1200  #f2060-2.0
        tiredeness_idx = 1208  #f2080-2.0
        date_idx = utils.fmri_date_14_idx
    else:
        depr_idx = 1197  #f2050-3.0
        disinterest_idx = 1201  #f2060-3.0
        tiredeness_idx = 1209  #f2080-3.0
        date_idx = utils.fmri_date_19_idx
    
    result = {}
    with tqdm(total=os.path.getsize(dataset_path)) as pbar:
        with open(dataset_path) as dataset_file:
            next(dataset_file)
            for i, line in enumerate(dataset_file):
                pbar.update(len(line))
                subject = line.lstrip().split('\t')
                eid = subject[0]
                date_str = subject[date_idx]
                depr = subject[depr_idx]
                disinterest = subject[disinterest_idx]
                tiredness = subject[tiredeness_idx]
                phq_items = [depr, disinterest, tiredness]
                # Check if non-empty
                if all(phq_item for phq_item in phq_items) and date_str:
                    phq_items = [int(phq_item) for phq_item in phq_items]
                    if all(phq_item > 0 for phq_item in phq_items):
                        total_phq_score = sum(phq_items)
                        date = datetime.strptime(date_str, '%Y-%m-%dT%H:%M:%S')
                        result[eid] = (date, total_phq_score - 4)  # Convert to 0-based coding

    if pickle_path:
        utils.save_to_pickle(result, pickle_path)
    return result

def get_high_phq_scores(dataset_path, threshold, pickle_path, instance, overwrite=False):
    phq_scores = get_total_phq_score(dataset_path, pickle_path, '2019', overwrite)
    high_phq_scores = {eid: info[0] for eid, info in phq_scores.items() if info[1] >= threshold}
    return high_phq_scores