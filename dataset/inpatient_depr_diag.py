import os
import datetime
import ukb_depr_utils as utils

from tqdm import tqdm
from pathlib import Path

'''
SQL statement used to acquire the bulk data specified by the path:

SELECT  hesin.eid, 
        hesin.ins_index, 
        epistart, 
        diag_icd10 
FROM    hesin JOIN hesin_diag using(eid, ins_index) 
WHERE   level IN (1,2) and 
        (diag_icd10 LIKE 'F32%' 
        or diag_icd10 LIKE 'F33%' 
        or diag_icd10 LIKE 'f34%' 
        or diag_icd10 LIKE 'F38%' 
        or diag_icd10 LIKE 'F39%')
'''
inpatient_depr_diag_path = Path(__file__).resolve().parent / 'ukb_bulk_data/inpatient_depr_diag.txt'

def get_inpatient_depr_diag(pickle_path, overwrite=False):
    if Path(pickle_path).is_file() and not overwrite:
        return utils.load_from_pickle(pickle_path)

    inpatient_depr_diag_subs = {}
    with tqdm(total=os.path.getsize(inpatient_depr_diag_path)) as pbar:
        with open(inpatient_depr_diag_path) as dataset_file:
            next(dataset_file)
            for i, line in enumerate(dataset_file):
                pbar.update(len(line))
                subject = line.lstrip().split('\t')
                eid = subject[0]
                date_str = subject[2]
                if date_str:
                    date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
                    inpatient_depr_diag_subs[eid] = date

    if pickle_path:
        utils.save_to_pickle(inpatient_depr_diag_subs, pickle_path)
    return inpatient_depr_diag_subs

if __name__ == '__main__':
    inpatient_depr_diag_pkl = 'temp/inpatient_depr_diag.pkl'
    inpatient_depr_diag_subs = get_inpatient_depr_diag(inpatient_depr_diag_pkl)
    print(len(inpatient_depr_diag_subs))