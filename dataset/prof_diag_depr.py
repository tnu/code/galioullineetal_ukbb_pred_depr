import ukb_depr_utils
import datetime

from pathlib import Path

def get_prof_diag_depr_mhq(dataset_path, pickle_path):
    prof_diag_start_idx = 11591
    prof_diag_end_idx = 11607
    depression_diagnosis_code = '11'
    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    prof_diag_depressed = {}
    with open(dataset_path) as dataset_file:
        next(dataset_file)
        for i, line in enumerate(dataset_file):
            if i % 10000 == 0:
                print(i)
            subject = line.split('\t')
            eid = subject[0]
            mhq_date_taken_str = subject[ukb_depr_utils.mhq_date_taken_idx]
            mhq_date_taken = datetime.datetime.strptime(mhq_date_taken_str, '%Y-%m-%d')
            for j in range(prof_diag_start_idx, prof_diag_end_idx):
                if subject[j] == depression_diagnosis_code:
                    prof_diag_depressed[eid] = mhq_date_taken

    ukb_depr_utils.save_to_pickle(prof_diag_depressed, pickle_path)
    return prof_diag_depressed

def get_not_prof_diag_mhq(dataset_path, pickle_path):
    ever_received_prof_help_idx = 11546
    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    not_prof_diag = {}
    with open(dataset_path) as dataset_file:
        next(dataset_file)
        for i, line in enumerate(dataset_file):
            if i % 10000 == 0:
                print(i)
            subject = line.split('\t')
            eid = subject[0]
            mhq_date_taken_str = subject[ukb_depr_utils.mhq_date_taken_idx]
            mhq_date_taken = datetime.datetime.strptime(mhq_date_taken_str, '%Y-%m-%d')
            if subject[ever_received_prof_help_idx] == '0':
                not_prof_diag[eid] = mhq_date_taken

    ukb_depr_utils.save_to_pickle(not_prof_diag, pickle_path)
    return not_prof_diag