import os
import datetime
import ukb_depr_utils

from pathlib import Path
from collections import defaultdict 
from tqdm import tqdm

main_dataset_path = 'ukb_main_data/main_dataset.txt'
coding4_path = Path(__file__).resolve().parent / 'ukb_bulk_data/coding4.tsv'
clinical_records_path = 'ukb_bulk_data/gp_scripts.txt'
pickles_dir = Path('./pickles')

# Source: https://www.nhs.uk/conditions/ssri-antidepressants/
ssri_drug_names = {'citalopram', 'escitalopram', 'fluoxetine', 'fluvoxamine', 'paroxetine', 'sertraline'}
ssri_brand_names = {'cipramil', 'prozac', 'oxactin', 'seroxat', 'lustral', 'cipralex', 'faverin'}
snri_drug_names = {'venlafaxine', 'duloxetine'}
snri_brand_names = {'efexor', 'cymbalta', 'yentreve'}
antidepressants = ssri_drug_names | ssri_brand_names | snri_drug_names | snri_brand_names

ukb_drug_dict = {}
with open(coding4_path) as coding4_file:
    next(coding4_file)
    for line in coding4_file:
        code_pair = line.split('\t')
        code = code_pair[0].strip()
        name = code_pair[1].strip()
        ukb_drug_dict[code] = name

def get_clinical_antidepr(clinical_records_path, pickle_path):
    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    clinical_antidepr = defaultdict(list)
    with open(clinical_records_path) as clinical_records_file:
        next(clinical_records_file)
        for i, line in enumerate(clinical_records_file):
            subject = line.split('\t')
            eid = subject[0]
            date_str = subject[2]
            drug_name = subject[6]
            antidepressant_match = any(antidepressant in drug_name for antidepressant in antidepressants)
            if antidepressant_match:
                date = datetime.datetime.strptime(date_str, '%d/%m/%Y')
                clinical_antidepr[eid].append(date)
            if i % 1000000 == 0:
                print(i)
                print(len(clinical_antidepr))
                print()
    
    ukb_depr_utils.save_to_pickle(clinical_antidepr, pickle_path)
    return clinical_antidepr

def get_latest_antidepr_date(clinical_antidepr):
    latest_clinical_antidepr = {}
    for eid in clinical_antidepr:
        dates = clinical_antidepr[eid]
        dates.sort(reverse=True)
        latest_clinical_antidepr[eid] = dates[0]
    return latest_clinical_antidepr

def get_antidepr_subjects(imaged_dataset_path, pickle_path, instance='2014'):
    if instance == '2014':
        imaged_date_idx = ukb_depr_utils.fmri_date_14_idx
        taking_drugs_idx = 1342  #f2492-2.0
        drug_start_idx = 8611  #f20003-2.0
        drug_end_idx = 8658  #f20003-2.47 (inclusive)
    else:
        imaged_date_idx = ukb_depr_utils.fmri_date_14_idx + 1
        taking_drugs_idx = 1343  #f2492-3.0
        drug_start_idx = 8659  #f20003-3.0
        drug_end_idx = 8706  #f20003-3.47 (inclusive)

    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    ssri_subjects = {}
    with tqdm(total=os.path.getsize(imaged_dataset_path)) as pbar:
        with open(imaged_dataset_path) as imaged_dataset_file:
            for i, line in enumerate(imaged_dataset_file):
                pbar.update(len(line))
                subject = line.split('\t')
                eid = subject[0]
                imaged_date = subject[imaged_date_idx]
                taking_drugs = subject[taking_drugs_idx]
                if taking_drugs == '1':
                    for j in range(drug_start_idx, drug_end_idx + 1):
                        subject_drug_code = subject[j]
                        if subject_drug_code:
                            subject_drug = ukb_drug_dict[subject_drug_code]
                            antidepressant_match = any(antidepressant in subject_drug for antidepressant in antidepressants)
                            if antidepressant_match:
                                ssri_subjects[eid] = (imaged_date, subject_drug)

    ukb_depr_utils.save_to_pickle(ssri_subjects, pickle_path)
    return ssri_subjects

def get_none_antidepr_subjects(imaged_dataset_path, pickle_path, instance='2014'):
    if instance == '2014':
        imaged_date_idx = ukb_depr_utils.fmri_date_14_idx
        taking_drugs_idx = 1342  #f2492-2.0
        drug_start_idx = 8611  #f20003-2.0
        drug_end_idx = 8658  #f20003-2.47 (inclusive)
    else:
        imaged_date_idx = ukb_depr_utils.fmri_date_14_idx + 1
        taking_drugs_idx = 1343  #f2492-3.0
        drug_start_idx = 8659  #f20003-3.0
        drug_end_idx = 8706  #f20003-3.47 (inclusive)

    if Path(pickle_path).is_file():
        return ukb_depr_utils.load_from_pickle(pickle_path)

    none_ssri_subjects = {}
    with open(imaged_dataset_path) as imaged_dataset_file:
        for i, line in enumerate(imaged_dataset_file):
            if i % 5000 == 0:
                print(i)
            subject = line.split('\t')
            eid = subject[0]
            imaged_date = subject[imaged_date_idx]
            taking_drugs = subject[taking_drugs_idx]
            if taking_drugs == '1':
                match = False
                for j in range(drug_start_idx, drug_end_idx + 1):
                    subject_drug_code = subject[j]
                    if subject_drug_code:
                        subject_drug = ukb_drug_dict[subject_drug_code]
                        antidepressant_match = any(antidepressant in subject_drug for antidepressant in antidepressants)
                        if antidepressant_match:
                            match = True
                if not match:
                    none_ssri_subjects[eid] = imaged_date
            elif taking_drugs == '0':
                none_ssri_subjects[eid] = imaged_date

    ukb_depr_utils.save_to_pickle(none_ssri_subjects, pickle_path)
    return none_ssri_subjects