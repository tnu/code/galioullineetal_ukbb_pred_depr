function write_features(ica_nr)
    rng default
    addpath('spm12');
    
    if ica_nr == 6 || ica_nr == 21
        control_files_dir = 'data_25/controls/*.txt';
        patient_files_dir = 'data_25/patients/*.txt';
    elseif ica_nr == 55
        control_files_dir = 'data_100_test/controls/*.txt';
        patient_files_dir = 'data_100_test/patients/*.txt';
    end
        
    control_files = dir(control_files_dir);
    patient_files = dir(patient_files_dir);

    num_controls = numel(control_files);
    num_patients = numel(patient_files);

    if ica_nr == 6
        % Stochastic DCM
%         stochastic_control_params = get_stochastic_params(control_files);
%         stochastic_patient_params = get_stochastic_params(patient_files);
%         stochastic_X = [reshape(stochastic_control_params, 36, num_controls)'; ...
%                         reshape(stochastic_patient_params, 36, num_patients)'];
%         writematrix(stochastic_X, "output/stochastic_X_6.csv");
% 
%         % Spectral DCM
%         spectral_control_params = get_spectral_params(control_files);
%         spectral_patient_params = get_spectral_params(patient_files);
%         spectral_X = [reshape(spectral_control_params, 36, num_controls)'; ...
%                       reshape(spectral_patient_params, 36, num_patients)'];
%         writematrix(spectral_X, "output/spectral_X_6.csv");
    end

    % rDCM
    rdcm_control_params = get_rdcm_params(control_files, ica_nr);
    rdcm_patient_params = get_rdcm_params(patient_files, ica_nr);
    rdcm_X = [reshape(rdcm_control_params, size(rdcm_control_params, 1)^2 , num_controls)'; ...
              reshape(rdcm_patient_params, size(rdcm_patient_params, 1)^2, num_patients)'];
    writematrix(rdcm_X, ['output/rdcm_', num2str(ica_nr), '_X.csv']);
    
    % Functional Connectivity
%     control_func_conn_features = get_func_conn_features(control_files, ica_nr);
%     patient_func_conn_features = get_func_conn_features(patient_files, ica_nr);
%     func_conn_X = [control_func_conn_features; patient_func_conn_features];
%     writematrix(func_conn_X, ['output/func_conn_', num2str(ica_nr), '_X.csv']);
end