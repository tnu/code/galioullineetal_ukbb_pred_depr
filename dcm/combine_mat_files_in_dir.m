function combine_mat_files_in_dir(params_dir)
    params_path = fileparts(strcat(params_dir, '//'));
    files = dir([params_path, '*.mat']);
    num_files = size(files, 1);
    
    result_path = fullfile(files(1).folder, 'all_params.mat');
    if isfile(result_path)
        disp([result_path, ' already exists'])
        return
    end
    
    all_params = zeros(6, 6, num_files);
    for fileIdx = 1:numel(files)
        if mod(fileIdx, 20) == 0
            disp([num2str(fileIdx), '/', num2str(numel(files))])
        end
        load(fullfile(files(fileIdx).folder, files(fileIdx).name), 'params');
        all_params(:, :, fileIdx) = params;
    end

    save(result_path, 'all_params')
end