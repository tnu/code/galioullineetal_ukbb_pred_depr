function all_params = get_rdcm_params(files, ica_nr)
    all_params_file_path = fullfile(files(1).folder, strcat('all_params_', num2str(ica_nr), '_rois.mat'));
    if isfile(all_params_file_path)
        all_params = importdata(all_params_file_path);
        return
    end
    num_files = length(files);
    all_params = zeros(ica_nr, ica_nr, num_files);
    for ts_file_idx = 1:num_files
        disp("Index: " + ts_file_idx)
        ts_file_path = fullfile(files(ts_file_idx).folder, files(ts_file_idx).name);
		
		all_timeseries = importdata(ts_file_path);
		if ica_nr == 6
			good_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
			roi_timeseries = good_timeseries(:, [1 3 5 6 13 21]);
		elseif ica_nr == 21
			roi_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
		else
			roi_timeseries = all_timeseries(:, [2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 ... 
				23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 45 46 48 49 50 52 53 57 58 60 63 64 93]);
		end
		
		Y.y = roi_timeseries; % BOLD time series for 6 ROIs
		Y.dt = 0.735;  % Interscan interval
		
		DCM = tapas_rdcm_model_specification(Y, [], []);
		DCM_est = tapas_rdcm_estimate(DCM, 'r', [], 1);
        all_params(:, :, ts_file_idx) = DCM_est.Ep.A;
    end
    save(all_params_file_path, 'all_params');
end