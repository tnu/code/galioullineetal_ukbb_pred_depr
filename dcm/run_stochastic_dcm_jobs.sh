#!/bin/bash

log_folder_name="bsub_logs_stochastic"

mkdir -p data/controls/$log_folder_name
mkdir -p data/controls/stochastic_params
mkdir -p data/patients/$log_folder_name
mkdir -p data/patients/stochastic_params

module load new matlab/R2019b

for filepath in data/controls/*.txt
do
  filename=${filepath##*/}
  bsub -J "$filepath" -R "rusage[mem=2048]" -o data/controls/${log_folder_name}/log_$filename matlab -nodisplay -singleCompThread -batch "get_single_stochastic_params('$filepath')"
done

for filepath in data/patients/*.txt
do
  filename=${filepath##*/}
  bsub -J "$filepath" -R "rusage[mem=2048]" -o data/controls/${log_folder_name}/log_$filename matlab -nodisplay -singleCompThread -batch "get_single_stochastic_params('$filepath')"
done
