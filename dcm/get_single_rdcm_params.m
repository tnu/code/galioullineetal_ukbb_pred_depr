function params = get_single_rdcm_params(file, ica_nr)
    addpath('spm12');
    addpath(genpath('rDCM'));

    all_timeseries = importdata(file);
    if ica_nr == 6
        good_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
        roi_timeseries = good_timeseries(:, [1 3 5 6 13 21]);
    elseif ica_nr == 21
        roi_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
    else
        roi_timeseries = all_timeseries(:, [2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 ... 
            23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 45 46 48 49 50 52 53 57 58 60 63 64 93]);
    end
    
    Y.y = roi_timeseries; % BOLD time series for 6 ROIs
    Y.dt = 0.735;  % Interscan interval
    
    DCM = tapas_rdcm_model_specification(Y, [], []);
    DCM_est = tapas_rdcm_estimate(DCM, 'r', [], 1);
    params = DCM_est.Ep.A;
end