function get_single_spectral_params(file)
    addpath('spm12');
    [path, name, ~] = fileparts(file);
    params_file_path = fullfile(path, 'spectral_params', [name, '_spectral_params.mat']);
    if isfile(params_file_path)
        disp([params_file_path, ' already exists'])
        return
    end

    all_timeseries = importdata(file);
    good_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
    roi_timeseries = good_timeseries(:, [1 3 5 6 13 21]);
    num_rois = size(roi_timeseries, 2);

    DCM.Y.y = roi_timeseries; % BOLD time series for 6 ROIs
    DCM.Y.dt = 0.735;  % Interscan interval
    DCM.Y.name = {'DMN', 'DAN', 'rFPN', 'lFPN', 'lCON', 'rCON'};
    for i = 1:num_rois
        DCM.xY(i).name = DCM.Y.name{i};
    end

    DCM.a = ones(num_rois, num_rois);
    DCM.U = struct();
    DCM.c = struct();
    DCM.U.u = [];

    DCM_est = spm_dcm_fmri_csd(DCM);
    params = DCM_est.Ep.A;

    save(params_file_path, 'params');
end