function all_features = get_func_conn_features(files, ica_nr)
    num_files = length(files);
    all_features = zeros(num_files, nchoosek(ica_nr + 1, 2) - ica_nr);
    for ts_file_idx = 1:num_files
        if mod(ts_file_idx, 20) == 0
            disp("Index: " + ts_file_idx)
        end
        ts_file_path = fullfile(files(ts_file_idx).folder, files(ts_file_idx).name);
        
        all_timeseries = importdata(ts_file_path);
        if ica_nr == 6
            good_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
            roi_timeseries = good_timeseries(:, [1 3 5 6 13 21]);
        elseif ica_nr == 21
            roi_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
        else
            roi_timeseries = all_timeseries(:, [2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 ... 
                23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 45 46 48 49 50 52 53 57 58 60 63 64 93]);
        end
        
        corr_matrix = corrcoef(roi_timeseries);
        % Only save relevant info
        features = nonzeros(triu(corr_matrix, 1));
        all_features(ts_file_idx, :) = features';
    end
end