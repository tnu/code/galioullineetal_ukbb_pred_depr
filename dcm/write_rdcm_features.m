function write_rdcm_features(control_roi_dir, patient_roi_dir, ica_nr)
    rng default
    addpath('spm12')
    addpath(genpath('rDCM'));
    
	if ~isfolder('output')
		mkdir output
	end
	
	control_files = dir([control_roi_dir '/*.txt']);
    patient_files = dir([patient_roi_dir '/*.txt']);

    num_controls = numel(control_files);
    num_patients = numel(patient_files);
	y = [num_controls * 2; zeros(num_controls, 1); ones(num_patients, 1)];  % With header
    writematrix(y, 'output/y_train.csv');
    
	rdcm_control_params = get_rdcm_params(control_files, ica_nr);
    rdcm_patient_params = get_rdcm_params(patient_files, ica_nr);
    rdcm_X = [reshape(rdcm_control_params, size(rdcm_control_params, 1)^2, num_controls)'; ...
              reshape(rdcm_patient_params, size(rdcm_patient_params, 1)^2, num_patients)'];
	rdcm_X = [1:size(rdcm_X, 2); rdcm_X];  % Add header
    writematrix(rdcm_X, ['output/rdcm_', num2str(ica_nr), '_X.csv']);
end