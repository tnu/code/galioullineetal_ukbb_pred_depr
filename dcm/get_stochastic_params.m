function stochastic_params = get_stochastic_params(files)
    params_file_path = fullfile(files(1).folder, 'stochastic_params/all_params.mat');
    if isfile(params_file_path)
        stochastic_params = importdata(params_file_path);
        return
    end
    num_files = length(files);
    stochastic_params = zeros(6, 6, num_files);
    parfor ts_file_idx = 1:num_files
        disp("Index: " + ts_file_idx)
        ts_file_path = fullfile(files(ts_file_idx).folder, files(ts_file_idx).name);
        all_timeseries = importdata(ts_file_path);
        good_timeseries = all_timeseries(:, [1  2  3  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22]);
        roi_timeseries = good_timeseries(:, [1 3 5 6 13 21]);

        DCM = struct();
        DCM.Y.y = roi_timeseries; % BOLD time series for 6 ROIs
        DCM.Y.dt = 0.735;  % Interscan interval
        DCM.Y.name = {'DMN', 'DAN', 'rFPN', 'lFPN', 'lCON', 'rCON'};
        for i = 1:6
            DCM.xY(i).name = DCM.Y.name{i};
        end

        DCM.a = ones(6,6);
        DCM.options.stochastic = 1;

        DCM.U = struct();
        DCM.c = struct();
        DCM.U.u = [];

        DCM_est = spm_dcm_estimate(DCM);
        conn_matrix = DCM_est.Ep.A;
        stochastic_params(:, :, ts_file_idx) = conn_matrix;
    end
    save(params_file_path, 'stochastic_params');
end